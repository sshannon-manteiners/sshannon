package com.example.sshannon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sshannon.server.Connection;
import com.example.sshannon.utils.SharedPreferencesHandler;

public class AddServerActivity extends AppCompatActivity {
    private TextView hostTv;
    private TextView userTv;
    private TextView passwordTv;
    private TextView portTv;

    private Button addServerButton;

    private Connection editingConnection;
    private boolean editing;

    private ProgressDialog progress;

    public AddServerActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_server);

        this.progress = new ProgressDialog(this);
        this.progress.setTitle(R.string.update_server_list_progress);
        this.progress.setCancelable(false);

        this.editing = false;

        this.hostTv = (TextView) findViewById(R.id.host_txt_area);
        this.userTv = (TextView) findViewById(R.id.user_txt_area);
        this.passwordTv = (TextView) findViewById(R.id.password_txt_area);
        this.portTv = (TextView) findViewById(R.id.port_txt_area);

        Intent intent = getIntent();
        String editingConnectionIdentifier = intent.getStringExtra("connection");
        if (editingConnectionIdentifier != null) {
            this.editing = true;

            this.editingConnection = SharedPreferencesHandler.fromStringToConnection(editingConnectionIdentifier);
            this.hostTv.setText(editingConnection.getHost());
            this.userTv.setText(editingConnection.getUser());
            this.passwordTv.setText(editingConnection.getPassword());
            this.portTv.setText(editingConnection.getPort());
        }

        this.addServerButton = (Button) findViewById(R.id.add_server_btn);
        if (this.editing)
            this.addServerButton.setText(R.string.edit_server);
        else
            this.addServerButton.setText(R.string.add_server);

        this.addServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewHost();
            }
        });
    }

    private void addNewHost() {
        this.progress.show();
        if (this.areFieldsEmpty()) {
            Toast.makeText(this, R.string.empty_field_toast, Toast.LENGTH_SHORT).show();
        } else {
            String host = String.valueOf(hostTv.getText()).trim();
            String user = String.valueOf(userTv.getText()).trim();
            String password = String.valueOf(passwordTv.getText()).trim();
            String port = String.valueOf(portTv.getText()).trim();

            Connection connection = new Connection(host, user, password, port);

            if (!this.editing) {
                this.addConnection(connection);

                Toast.makeText(this, this.getResources().getString(R.string.add_server) + host, Toast.LENGTH_SHORT).show();
            } else {
                SharedPreferencesHandler.editConnection(this.editingConnection, connection, this.getBaseContext());
            }

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        this.progress.dismiss();
    }

    private void addConnection(Connection connection) {
        SharedPreferencesHandler.addConnection(connection, this.getBaseContext());
    }

    private boolean areFieldsEmpty() {
        if (TextUtils.isEmpty(String.valueOf(hostTv.getText()).trim()) || String.valueOf(hostTv.getText()).trim().length() == 0 || String.valueOf(hostTv.getText()).trim().equals("") ||
                TextUtils.isEmpty(String.valueOf(userTv.getText()).trim()) || String.valueOf(userTv.getText()).trim().length() == 0 || String.valueOf(userTv.getText()).trim().equals("") ||
                TextUtils.isEmpty(String.valueOf(passwordTv.getText()).trim()) || String.valueOf(passwordTv.getText()).trim().length() == 0 || String.valueOf(passwordTv.getText()).trim().equals("") ||
                TextUtils.isEmpty(String.valueOf(portTv.getText()).trim()) || String.valueOf(portTv.getText()).trim().length() == 0 || String.valueOf(portTv.getText()).trim().equals(""))
            return true;
        else
            return false;
    }
}
