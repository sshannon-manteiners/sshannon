package com.example.sshannon;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InfoActivity extends AppCompatActivity {
    private TextView infoTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        try {
            this.infoTV = (TextView) this.findViewById(R.id.info_tv);
            this.infoTV.setText(readInfoFromJSON(this));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static String readText(Context context, int resId) throws IOException {
        InputStream is = context.getResources().openRawResource(resId);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String s = null;
        while ((s = br.readLine()) != null) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    public String readInfoFromJSON(Context context) throws IOException, JSONException {
        String jsonText = readText(context, R.raw.info);
        JSONObject jsonRoot = new JSONObject(jsonText);

        JSONArray jsonArray = jsonRoot.getJSONArray("developers");
        Developer[] developers = new Developer[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            String name = jsonArray.getJSONObject(i).getString("name");
            String id = jsonArray.getJSONObject(i).getString("id");
            developers[i] = new Developer(name, id);
        }

        String appName = jsonRoot.getString("appName");
        String exam = jsonRoot.getString("exam");
        String prof = jsonRoot.getString("prof");
        String date = jsonRoot.getString("date");

        InfoPrinter infoPrinter = new InfoPrinter(developers, appName, exam, prof, date);
        return infoPrinter.toString();
    }
}

class InfoPrinter {
    private Developer[] developers;
    private String appName;
    private String exam;
    private String prof;
    private String date;

    public InfoPrinter(Developer[] developers, String appName, String exam, String prof, String date) {
        this.developers = developers;
        this.appName = appName;
        this.exam = exam;
        this.prof = prof;
        this.date = date;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(this.appName + "\n\n");
        sb.append(this.exam + "\n");
        sb.append(this.prof + "\n\n");
        sb.append(this.date + "\n\n");

        sb.append("Developers:\n");
        for (Developer dev : this.developers)
            sb.append(dev.getName() + ", " + dev.getId() + "\n");

        return sb.toString();
    }
}

class Developer {
    private String name;
    private String id;

    public Developer(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
