package com.example.sshannon;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.example.sshannon.server.Connection;
import com.example.sshannon.utils.SharedPreferencesHandler;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Connection> {
    private MainActivity activity;
    private List<Connection> connections;


    public ListViewAdapter(MainActivity context, int resource, List<Connection> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.connections = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        // If holder not exist then locate all view from UI file.
        if (convertView == null) {
            // inflate UI from XML file
            convertView = inflater.inflate(R.layout.item_listview, parent, false);
            // get all UI view
            holder = new ViewHolder(convertView);
            // set tag for holder
            convertView.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) convertView.getTag();
        }

        holder.icon.setImageResource(holder.getIcon(getItem(position).getIcon()));
        holder.name.setText(getItem(position).getHost());
        holder.user.setText("User: " + getItem(position).getUser());
        holder.port.setText("Port: " + getItem(position).getPort());

        //handling buttons event
        holder.btnEdit.setOnClickListener(onEditListener(position, holder));
        holder.btnDelete.setOnClickListener(onDeleteListener(position, holder));

        // adding listener to open connection
        LinearLayout surfaceLayout = convertView.findViewById(R.id.surface_view);
        surfaceLayout.setOnClickListener(onOpenConnectionListener(position, holder));

        return convertView;
    }

    private View.OnClickListener onEditListener(final int position, final ViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEditPage(connections.get(position));
            }
        };
    }

    private View.OnClickListener onDeleteListener(final int position, final ViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle(R.string.dialog_delete_connection_title)
                        .setMessage(R.string.dialog_delete_connection_message)
                        .setPositiveButton(R.string.dialog_delete_connection_confirm, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                deleteFromSharedPreferences(connections.get(position));
                                connections.remove(position);
                                holder.swipeLayout.close();
                                activity.updateAdapter();
                                Toast.makeText(activity, R.string.dialog_delete_connection_toast_confirm, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(R.string.dialog_delete_connection_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(activity, R.string.dialog_delete_connection_toast_cancel, Toast.LENGTH_SHORT).show();
                            }
                        });
                builder.create().show();
            }
        };
    }

    private View.OnClickListener onOpenConnectionListener(final int position, final ViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.openServer(connections.get(position));
            }
        };
    }

    private void openEditPage(Connection connection) {
        Intent intent = new Intent(activity, AddServerActivity.class);
        intent.putExtra("connection", connection.createConnectionIdentifier());
        activity.startActivity(intent);
    }

    private class ViewHolder {
        int[] icons = new int[]{
                R.drawable.arch,
                R.drawable.centos,
                R.drawable.debian,
                R.drawable.fedora,
                R.drawable.freebsd,
                R.drawable.mac,
                R.drawable.mageia,
                R.drawable.mintos,
                R.drawable.opensuse,
                R.drawable.redhat,
                R.drawable.slackware,
                R.drawable.ubuntu,
                R.drawable.linux,
                R.drawable.default_server
        };

        private TextView name;
        private TextView port;
        private TextView user;
        private ImageView icon;
        private View btnDelete;
        private View btnEdit;
        private SwipeLayout swipeLayout;

        public ViewHolder(View v) {
            swipeLayout = (SwipeLayout) v.findViewById(R.id.swipe_layout);
            btnDelete = v.findViewById(R.id.delete);
            btnEdit = v.findViewById(R.id.edit_query);
            name = (TextView) v.findViewById(R.id.name);
            port = (TextView) v.findViewById(R.id.port);
            user = (TextView) v.findViewById(R.id.user);
            icon = v.findViewById(R.id.server_icon);

            swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        }

        public int getIcon(int pos) {
            return icons[pos];
        }
    }

    private void deleteFromSharedPreferences(Connection connection) {
        SharedPreferencesHandler.deleteConnection(connection, this.getContext());
    }
}