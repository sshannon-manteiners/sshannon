package com.example.sshannon;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.sshannon.server.Connection;
import com.example.sshannon.utils.SharedPreferencesHandler;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_STORAGE = 1;
    public static final int REQUEST_CODE_INTERNET = 2;

    private Button newConnectionButton;
    private Button infoButton;

    private ListView listView;
    private ArrayAdapter<Connection> adapter;
    private ArrayList<Connection> connectionsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_STORAGE);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, REQUEST_CODE_INTERNET);

        this.newConnectionButton = (Button) findViewById(R.id.new_connection_button);
        this.newConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddServerActivity();
            }
        });

        this.infoButton = (Button) findViewById(R.id.info_button);
        this.infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInfoActivity();
            }
        });

        this.listView = (ListView) findViewById(R.id.list_item);

        this.connectionsList = this.getConnections();

        this.setListViewAdapter();
        this.updateAdapter();
    }

    public void openServer(Connection connection) {
        Intent intent = new Intent(this, ServerNavigationActivity.class);
        intent.putExtra("connection", connection.getIntentData());
        startActivity(intent);
    }

    private void openAddServerActivity() {
        Intent intent = new Intent(this, AddServerActivity.class);
        startActivity(intent);
    }

    private void openInfoActivity() {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }

    private void setListViewAdapter() {
        this.adapter = new ListViewAdapter(this, R.layout.item_listview, connectionsList);
        this.listView.setAdapter(adapter);
    }

    public void updateAdapter() {
        this.adapter.notifyDataSetChanged(); //update adapter
    }

    public ArrayList<Connection> getConnections() {
        ArrayList<Connection> returningArray = SharedPreferencesHandler.getConnections(this.getBaseContext());

        return returningArray;
    }

    @Override
    public void onBackPressed() {
    }
}
