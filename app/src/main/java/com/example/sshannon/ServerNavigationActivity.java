package com.example.sshannon;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sshannon.server.CommandExecuter;
import com.example.sshannon.server.Connection;
import com.example.sshannon.server.File;
import com.example.sshannon.server.PathManager;
import com.example.sshannon.server.Result;
import com.example.sshannon.services.WatchService;
import com.example.sshannon.utils.Constants;
import com.example.sshannon.utils.SharedPreferencesHandler;
import com.example.sshannon.utils.WatchHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.sshannon.server.Connection.*;
import static com.example.sshannon.utils.Constants.NO_CONNECTION_STR;

public class ServerNavigationActivity extends AppCompatActivity {
    private static final int PICKFILE_RESULT_CODE = 1;

    //
    private static final int RM_DIR = 0;
    //ALERT MENU SWITCH OPTIONS
    private static final int DOWNLOAD = 0;
    private static final int DELETE = 1;

    //ICON ARRAY POSITIONS
    private static final int DIRECTORY = 0;
    private static final int GENERIC_FILE = 1;
    private static final int IMAGE = 2;
    private static final int PDF = 3;
    private static final int TXT = 4;

    private Button backButton;
    private Button homeButton;
    private Button newDirButton;
    private Button uploadButton;
    private Button watchButton;

    private Connection selectedConnection;
    private CommandExecuter commandExecuter;
    private PathManager pathManager;

    private String[] intentData;
    private ArrayList<File> structure;

    private String updateFilePath;

    private List<HashMap<String, String>> aList;

    private TextView pathTv;
    private ListView listView;
    private SimpleAdapter adapter;
    private ProgressDialog loading;

    private Handler handler;
    private String newDirName;

    // Array of integers points to images stored in /res/drawable-ldpi/
    int[] icons = new int[]{
            R.drawable.folder,
            R.drawable.genericfile,
            R.drawable.image,
            R.drawable.pdf,
            R.drawable.txt
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_navigation);

        this.handler = new Handler();

        this.loading = new ProgressDialog(this);
        loading.setTitle(R.string.connection_loading_title);
        loading.setMessage(this.getResources().getString(R.string.connection_loading_message));
        loading.setCancelable(false);

        this.commandExecuter = new CommandExecuter();

        this.backButton = findViewById(R.id.back_btn);
        this.backButton.setOnClickListener(this.backButtonListener());

        this.homeButton = findViewById(R.id.home_btn);
        this.homeButton.setOnClickListener(this.homeButtonListener());

        this.newDirButton = findViewById(R.id.newdir_btn);
        this.newDirButton.setOnClickListener(this.newDirectoryButtonListener());

        this.uploadButton = findViewById(R.id.upload_btn);
        this.uploadButton.setOnClickListener(this.uploadFileButtonListener());

        this.watchButton = findViewById(R.id.watch_btn);
        this.watchButton.setOnClickListener(this.watchButtonListener());

        this.structure = new ArrayList<>();
        this.aList = new ArrayList<>();

        this.pathTv = findViewById(R.id.path_text);
        this.listView = findViewById(R.id.listview);
        this.listView.setClickable(true);

        Intent intent = getIntent();
        this.intentData = intent.getStringArrayExtra("connection");

        this.selectedConnection = new Connection(intentData[HOST], intentData[USER], intentData[PASSWORD], intentData[PORT]);
        this.selectedConnection.setIcon(Integer.parseInt(intentData[ICON]));
        final Connection updatedConnection = new Connection(selectedConnection.getHost(), selectedConnection.getUser(), selectedConnection.getPassword(), selectedConnection.getPort());

        if (this.selectedConnection.getIcon() == Constants.DEFAULT) {
            final Result result = new Result();
            this.commandExecuter.executeCommand(selectedConnection, "uname -s | tr '[:upper:]' '[:lower:]' && hostnamectl | grep 'Operating System'", result, this.getApplicationContext());
            new Thread(new Runnable() {
                public void run() {
                    while (result.isWaiting()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (result.getOutput().equals(NO_CONNECTION_STR)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                connectionAbsent();
                            }
                        });
                    } else {
                        updatedConnection.setIcon(selectedConnection.recognizeOs(result.getOutput().toLowerCase()));
                        SharedPreferencesHandler.editConnection(selectedConnection, updatedConnection, getBaseContext());
                    }
                }
            }).start();
        }
        this.getRootPoint();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //this.stopWatchService();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICKFILE_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                String FilePath = data.getData().getPath();
                String FileName = data.getData().getLastPathSegment();
                updateFilePath = data.getData().getLastPathSegment();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("RESUME*");
    }

    private void connectionAbsent() {
        Toast.makeText(getApplicationContext(), NO_CONNECTION_STR, Toast.LENGTH_LONG).show();
        loading.dismiss();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    public void startWatchService() {
        final Result result = new Result();
        this.commandExecuter.executeCommand(this.selectedConnection, "ls " + pathManager.getCurrentPath(), result, this.getApplicationContext());
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (result.isWaiting()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Intent serviceIntent = new Intent(getApplicationContext(), WatchService.class);
                String watchString = WatchHandler.createWatchId(selectedConnection, pathManager.getCurrentPath(), result.getOutput());
                serviceIntent.putExtra("watch", watchString);
                startService(serviceIntent);
            }
        }).start();
    }

    public void stopWatchService() {
        Intent serviceIntent = new Intent(this, WatchService.class);
        stopService(serviceIntent);
    }

    private View.OnClickListener backButtonListener() {
        if (commandExecuter.isInternetAvailable()) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goBack();
                }
            };
        } else {
            connectionAbsent();
            return null;
        }
    }

    private View.OnClickListener homeButtonListener() {
        if (commandExecuter.isInternetAvailable()) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    returnHome();
                }
            };
        } else {
            connectionAbsent();
            return null;
        }
    }

    private View.OnClickListener newDirectoryButtonListener() {
        if (commandExecuter.isInternetAvailable()) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ServerNavigationActivity.this);
                    builder.setTitle(R.string.new_folder_dialog_title);

                    final EditText input = new EditText(ServerNavigationActivity.this);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            newDirName = input.getText().toString();
                            newDirectory(newDirName);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }
            };
        } else {
            connectionAbsent();
            return null;
        }
    }

    private View.OnClickListener uploadFileButtonListener() {
        if (commandExecuter.isInternetAvailable()) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uploadFile();
                }
            };
        } else {
            connectionAbsent();
            return null;
        }
    }

    private View.OnClickListener watchButtonListener() {
        if (commandExecuter.isInternetAvailable()) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "Start watching: " + pathManager.getCurrentPath(), Toast.LENGTH_LONG).show();
                    startWatchService();
                }
            };
        } else {
            connectionAbsent();
            return null;
        }
    }

    private void getRootPoint() {
        final Result result = new Result();
        this.loading.show();

        commandExecuter.executeCommand(selectedConnection, "pwd", result, this.getApplicationContext());

        new Thread(new Runnable() {
            public void run() {
                while (result.isWaiting()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (result.getOutput().equals(NO_CONNECTION_STR)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectionAbsent();
                        }
                    });
                } else {
                    pathManager = new PathManager(result.getOutput().replace("\n", ""));
                    getStructure();
                }
            }
        }).start();
    }

    private void getStructure() {
        final Result result = new Result();
        this.loading.show();

        commandExecuter.executeCommand(selectedConnection, "ls -l " + this.pathManager.getCurrentPath(), result, this.getApplicationContext());

        new Thread(new Runnable() {
            public void run() {
                while (result.isWaiting()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                /*if (result.getOutput().equals(NO_CONNECTION_STR)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectionAbsent();
                        }
                    });
                } else {*/
                Log.d("Output: ", result.getOutput());
                parseString(result.getOutput());
                refreshScreen();
                loading.dismiss();
                //}
            }
        }).start();
    }

    private void refreshScreen() {
        this.handler.post(new Runnable() {
            @Override
            public void run() {
                for (File _file : structure) {
                    if (_file.getName().equals(".") || _file.getName().equals("..")) {
                        continue;
                    }
                    HashMap<String, String> hm = new HashMap<String, String>();
                    hm.put("fileName", _file.getName());
                    hm.put("icon", Integer.toString(icons[getIcon(_file.getType())]));
                    hm.put("type", _file.getType());
                    aList.add(hm);
                }

                String[] from = {"icon", "fileName", "type"};
                int[] to = {R.id.icon, R.id.filename};
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Object o = listView.getItemAtPosition(position);
                        HashMap<String, String> hm = (HashMap<String, String>) o;
                        final String fileName = hm.get("fileName");
                        String fileType = hm.get("type");
                        if (fileType.equals(File.DIRECTORY)) {
                            aList.clear();
                            structure.clear();
                            adapter.notifyDataSetChanged();
                            changeDirectory(fileName);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ServerNavigationActivity.this);
                            builder.setTitle(fileName);
                            builder.setItems(new CharSequence[]{
                                            ServerNavigationActivity.this.getText(R.string.on_click_file_download),
                                            ServerNavigationActivity.this.getText(R.string.on_click_file_delete)},
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // The 'which' argument contains the index position
                                            // of the selected item
                                            if (commandExecuter.isInternetAvailable()) {
                                                Log.d("File selected: ", pathManager.getCurrentPath() + "/" + fileName);
                                                switch (which) {
                                                    case DOWNLOAD:
                                                        downloadFile(pathManager.getCurrentPath(), fileName);
                                                        break;
                                                    case DELETE:
                                                        removeFile(pathManager.getCurrentPath() + "/" + fileName);
                                                        break;
                                                }
                                            } else {
                                                connectionAbsent();
                                            }
                                        }
                                    });
                            builder.create().show();
                        }
                    }
                });

                listView.setLongClickable(true);
                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        Object o = listView.getItemAtPosition(position);
                        HashMap<String, String> hm = (HashMap<String, String>) o;
                        final String fileName = hm.get("fileName");
                        final String type = hm.get("type");
                        if (type.equals(File.DIRECTORY)) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ServerNavigationActivity.this);
                            builder.setTitle(fileName);
                            builder.setItems(new CharSequence[]{
                                            ServerNavigationActivity.this.getText(R.string.on_long_click_dir_delete)},
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (commandExecuter.isInternetAvailable()) {
                                                switch (which) {
                                                    case RM_DIR:
                                                        Log.d("Dirname", pathManager.getCurrentPath() + "/" + fileName);
                                                        removeDirectory(pathManager.getCurrentPath() + "/" + fileName);
                                                        break;
                                                }
                                            } else
                                                connectionAbsent();
                                        }
                                    });
                            builder.create().show();
                        }
                        return true;
                    }
                });
                adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);
                adapter.notifyDataSetChanged();
                listView.setAdapter(adapter);
                pathTv.setText(pathManager.getCurrentPath());
            }
        });
    }

    private void changeDirectory(String dirName) {
        if (pathManager.getCurrentPath().equals("/")) {
            this.pathManager.updatePath(pathManager.getCurrentPath() + dirName);
        } else {
            this.pathManager.updatePath(pathManager.getCurrentPath().concat("/") + dirName);
        }
        this.getStructure();
    }

    private void goBack() {
        if (pathManager.getCurrentPath().equals(pathManager.getHomePath())) {
            return;
        } else {
            pathManager.goBack();
            this.aList.clear();
            this.structure.clear();
            this.getStructure();
        }
    }

    private void returnHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void downloadFile(String path, final String fileName) {
        Toast.makeText(getApplicationContext(), "Preparing download " + fileName + "...", Toast.LENGTH_SHORT).show();
        final Result result = new Result();
        this.commandExecuter.downloadFileSftp(this.selectedConnection, path, fileName, result);
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (result.isWaiting()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }).join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), fileName + " downloaded successfully!", Toast.LENGTH_SHORT).show();
    }

    private void removeFile(String filePath) {
        final Result result = new Result();
        this.loading.show();
        this.commandExecuter.executeCommand(this.selectedConnection, "rm " + filePath, result, this.getApplicationContext());
        new Thread(new Runnable() {
            public void run() {
                while (result.isWaiting()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (result.getOutput().equals(NO_CONNECTION_STR)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectionAbsent();
                        }
                    });
                } else {
                    aList.clear();
                    structure.clear();
                    getStructure();
                }
            }
        }).start();
    }

    private void newDirectory(String dirName) {
        final Result result = new Result();
        this.loading.show();
        this.commandExecuter.executeCommand(this.selectedConnection, "mkdir " + pathManager.getCurrentPath() + "/" + dirName, result, this.getApplicationContext());
        new Thread(new Runnable() {
            public void run() {
                while (result.isWaiting()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (result.getOutput().equals(NO_CONNECTION_STR)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectionAbsent();
                        }
                    });
                } else {
                    aList.clear();
                    structure.clear();
                    getStructure();
                }
            }
        }).start();
    }

    private void removeDirectory(String dirName) {
        final Result result = new Result();
        loading.show();
        this.commandExecuter.executeCommand(this.selectedConnection, "rm -rf " + dirName, result, this.getApplicationContext());
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (result.isWaiting()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (result.getOutput().equals(NO_CONNECTION_STR)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectionAbsent();
                        }
                    });
                } else {
                    aList.clear();
                    structure.clear();
                    getStructure();
                }
            }
        }).start();
    }

    private void uploadFile() {
        final Result nestedResult = new Result();
        try {
            updateFilePath = null;
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(intent, PICKFILE_RESULT_CODE);
        } catch (ActivityNotFoundException exp) {
            Toast.makeText(getBaseContext(), "No File (Manager / Explorer)etc Found In Your Device", Toast.LENGTH_LONG).show();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (updateFilePath == null) {
                    try {
                        Thread.sleep(100);
                        System.out.println(updateFilePath);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println
                        ("FILE: " + updateFilePath.replace("raw:", ""));
                commandExecuter.uploadFileSftp(selectedConnection, pathManager.getCurrentPath() + "/", updateFilePath.replace("raw:", ""), nestedResult);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (nestedResult.isWaiting()) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                }).start();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        aList.clear();
                        structure.clear();
                        getStructure();
                    }
                });
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Operation not allowed here.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }).start();
    }

    private void parseString(String output) {
        String[] parsedString = output.split("\n");
        File file;
        for (int i = 0; i < parsedString.length; i++) {
            Log.i("Output  ", parsedString[i]);
            file = new File(parsedString[i]);
            structure.add(file);
        }
    }

    private int getIcon(String fileType) {
        switch (fileType) {
            case File.DIRECTORY:
                return DIRECTORY;
            case File.IMAGE:
                return IMAGE;
            case File.PDF:
                return PDF;
            case File.TEXT:
                return TXT;
            default:
                return GENERIC_FILE;
        }
    }

    @Override
    public void onBackPressed() {
    }
}

