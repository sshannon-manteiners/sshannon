package com.example.sshannon.server;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.example.sshannon.utils.ProgressMonitor;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.example.sshannon.utils.Constants.NO_CONNECTION_STR;

public class CommandExecuter {

    private static final String CONNECTION_NOT_FOUND = "Connection not found";
    private static final int SESSION_TIMEOUT = 10000;
    private static final int CHANNEL_TIMEOUT = 5000;

    private String output;

    @SuppressLint("StaticFieldLeak")
    public void executeCommand(final Connection connection, final String command, final Result result, final Context context) {
        if (this.isInternetAvailable()) {
            new AsyncTask<Integer, Void, Void>() {

                @Override
                protected Void doInBackground(Integer... params) {

                    Session session = null;
                    Channel channel = null;
                    ChannelSftp channelSftp = null;

                    try {
                        JSch jsch = new JSch();
                        session = jsch.getSession(connection.getUser(), connection.getHost(), Integer.parseInt(connection.getPort()));
                        session.setPassword(connection.getPassword());
                        session.setConfig("StrictHostKeyChecking", "no");
                        session.setTimeout(SESSION_TIMEOUT);
                        session.connect();

                        ChannelExec channel1 = (ChannelExec) session.openChannel("exec");
                        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        channel1.setOutputStream(baos);
                        channel1.setCommand(command);
                        channel1.connect(CHANNEL_TIMEOUT);
                        try {
                            Thread.sleep(1000);
                        } catch (Exception ee) {
                        }
                        Log.d("Exit Status: ", String.valueOf(channel1.getExitStatus()));
                        output = new String(baos.toByteArray());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void unused) {
                    if (output == null) {
                        output = CONNECTION_NOT_FOUND;
                    }
                    result.setOutput(output);
                    result.setWaiting(false);
                }
            }.execute(1);
        } else {
            result.setOutput(NO_CONNECTION_STR);
            result.setWaiting(false);
        }
    }


    @SuppressLint("StaticFieldLeak")
    public void downloadFileSftp(final Connection connection, final String path, final String fileName, final Result result) {

        new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... params) {

                try {
                    JSch ssh = new JSch();
                    Session session = ssh.getSession(connection.getUser(), connection.getHost(), Integer.parseInt(connection.getPort()));
                    // Remember that this is just for testing and we need a quick access, you can add an identity and known_hosts file to prevent
                    // Man In the Middle attacks
                    java.util.Properties config = new java.util.Properties();
                    config.put("StrictHostKeyChecking", "no");
                    session.setConfig(config);
                    session.setPassword(connection.getPassword());

                    session.connect();
                    Channel channel = session.openChannel("sftp");
                    channel.connect();

                    ChannelSftp sftp = (ChannelSftp) channel;

                    //sftp.cd(directory);
                    // If you need to display the progress of the upload, read how to do it in the end of the article

                    // use the put method , if you are using android remember to remove "file://" and use only the relative path
                    Log.d("Path", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
                    sftp.get(path + "/" + fileName, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/", new ProgressMonitor());

                    channel.disconnect();
                    session.disconnect();
                } catch (JSchException e) {
                    System.out.println(e.getMessage().toString());
                    e.printStackTrace();
                } catch (SftpException e) {
                    System.out.println(e.getMessage().toString());
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void unused) {
                result.setWaiting(false);
            }
        }.execute(1);
    }

    @SuppressLint("StaticFieldLeak")
    public void uploadFileSftp(final Connection connection, final String path, final String filePath, final Result result) {

        new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    JSch ssh = new JSch();
                    Session session = ssh.getSession(connection.getUser(), connection.getHost(), Integer.parseInt(connection.getPort()));
                    // Remember that this is just for testing and we need a quick access, you can add an identity and known_hosts file to prevent
                    // Man In the Middle attacks
                    java.util.Properties config = new java.util.Properties();
                    config.put("StrictHostKeyChecking", "no");
                    session.setConfig(config);
                    session.setPassword(connection.getPassword());

                    session.connect();
                    Channel channel = session.openChannel("sftp");
                    channel.connect();

                    ChannelSftp sftp = (ChannelSftp) channel;
                    //sftp.cd(directory);


                    sftp.put(filePath, path + "/");

                    channel.disconnect();
                    session.disconnect();
                } catch (JSchException e) {
                    System.out.println(e.getMessage().toString());
                    e.printStackTrace();
                } catch (SftpException e) {
                    System.out.println(e.getMessage().toString());
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void unused) {
                result.setWaiting(false);
            }
        }.execute(1);
    }

    public boolean isInternetAvailable() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}
