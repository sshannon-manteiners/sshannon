package com.example.sshannon.server;

import static com.example.sshannon.utils.Constants.*;

public class Connection {

    private static final int DEFAULT_ICON = 13;

    public static final int FTP_PORT = 21;
    public static final int SSH_PORT = 22;

    private static final int DATA = 5;
    public static final int HOST = 0;
    public static final int USER = 1;
    public static final int PASSWORD = 2;
    public static final int PORT = 3;
    public static final int ICON = 4;

    private String host;
    private String user;
    private String password;
    private String port;
    private int icon;

    private String[] intentData;

    public Connection(String pHost, String pUser, String pPassword, String pPort) {
        this.host = pHost;
        this.user = pUser;
        this.password = pPassword;
        this.port = pPort;
        this.icon = DEFAULT_ICON;

        this.intentData = new String[DATA];
        this.intentData[HOST] = this.host;
        this.intentData[USER] = this.user;
        this.intentData[PASSWORD] = this.password;
        this.intentData[PORT] = this.port;
        this.intentData[ICON] = String.valueOf(this.icon);
    }

    public Connection(String pHost, String pUser, String pPassword, String pPort, int icon) {
        this(pHost, pUser, pPassword, pPort);
        this.icon = icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public String getHost() {
        return this.host;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getPort() {
        return port;
    }

    public String[] getIntentData() {
        return intentData;
    }

    public String createConnectionIdentifier() {
        StringBuilder retString = new StringBuilder();
        retString.append(this.host + "|");
        retString.append(this.user + "|");
        retString.append(this.password + "|");
        retString.append(this.port + "|");
        retString.append(this.icon);
        return retString.toString();
    }

    public int recognizeOs(String uname) {
        if (uname.contains("linux")) {
            if (uname.contains("ARCH")) {
                return ARCH;
            } else if (uname.contains("centos")) {
                return CENTOS;
            } else if (uname.contains("debian")) {
                return DEBIAN;
            } else if (uname.contains("fedora")) {
                return FEDORA;
            } else if (uname.contains("freebsd") || uname.contains("bsd")) {
                return FREEBSD;
            } else if (uname.contains("mageia")) {
                return MAGEIA;
            } else if (uname.contains("mint")) {
                return MINTOS;
            } else if (uname.contains("opensuse")) {
                return OPENSUSE;
            } else if (uname.contains("redhat")) {
                return REDHAT;
            } else if (uname.contains("slackware")) {
                return SLACKWARE;
            } else if (uname.contains("ubuntu")) {
                return UBUNTU;
            } else {
                return LINUX;
            }
        } else if(uname.contains("darwin"))
            return MAC;
        else
            return LINUX;
    }
}
