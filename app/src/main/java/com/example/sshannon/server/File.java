package com.example.sshannon.server;

import android.util.Log;

public class File {

    //FILES EXTENSION
    private static final String TXT_FILE = "txt";
    private static final String PDF_FILE = "pdf";
    private static final String IMG_FILE = "img";
    private static final String PNG_FILE = "png";
    private static final String JPG_FILE = "jpg";
    private static final String JPEG_FILE = "jpeg";


    //FILE TYPE
    public static final String DIRECTORY = "dir";
    public static final String TEXT = "txt";
    public static final String IMAGE = "img";
    public static final String PDF = "pdf";
    public static final String GENERAL_FILE = "general";

    private static final int SPLIT_POSITION = 8;

    private String name;
    private String type;

    public File(String pName) {
        String[] s = pName.split("\\s+");
        if (s.length > SPLIT_POSITION) {
            this.name = "";
            for (int i = SPLIT_POSITION; i < s.length; i++) {
                name = name + s[i];
            }
            if (pName.charAt(0) == 'd') {
                this.type = DIRECTORY;
            } else {
                this.type = this.getFileType(this.name);
            }
        } else {
            this.name = ".";
            this.type = null;
        }
    }


    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    private String getFileType(String fileName) {
        Log.i("Filename ", fileName);
        String[] stringSplitted = fileName.split("\\.");
        if (stringSplitted.length != 0) {
            switch (stringSplitted[stringSplitted.length - 1]) {
                case TXT_FILE:
                    return TEXT;

                case PDF_FILE:
                    return PDF;

                case PNG_FILE:
                case JPG_FILE:
                case IMG_FILE:
                case JPEG_FILE:
                    return IMAGE;

                default:
                    return GENERAL_FILE;
            }
        } else {
            return "";
        }
    }

    @Override
    public String toString() {
        return ("Name: " + this.name + ", Type: " + this.type);
    }
}
