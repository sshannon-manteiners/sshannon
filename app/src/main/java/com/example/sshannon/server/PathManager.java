package com.example.sshannon.server;

import java.util.Stack;

public class PathManager {

    private Stack<String> path;

    public PathManager(String pHomePath) {
        this.path = new Stack<>();
        this.path.push(pHomePath);
    }

    public void updatePath(String step) {
        this.path.push(step);
    }

    public String getHomePath() {
        return path.get(0);
    }

    public void goBack() {
        this.path.pop();
    }

    public String getCurrentPath() {
        return this.path.lastElement();
    }
}
