package com.example.sshannon.server;

public class Result {

    private String output;
    private boolean isWaiting;

    public Result() {
        this.output = null;
        this.isWaiting = true;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public boolean isWaiting() {
        return isWaiting;
    }

    public void setWaiting(boolean waiting) {
        isWaiting = waiting;
    }
}
