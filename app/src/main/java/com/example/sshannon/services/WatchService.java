package com.example.sshannon.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.example.sshannon.R;
import com.example.sshannon.ServerNavigationActivity;
import com.example.sshannon.server.CommandExecuter;
import com.example.sshannon.server.Connection;
import com.example.sshannon.server.Result;
import com.example.sshannon.utils.Constants;

import static com.example.sshannon.services.ShannonNotification.CHANNEL_ID;
import static com.example.sshannon.utils.Constants.NO_CONNECTION_STR;
import static com.example.sshannon.utils.Constants.PATH_POSITION;

public class WatchService extends Service {

    private NotificationManagerCompat notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        this.notificationManager = NotificationManagerCompat.from(this);
        Log.i("NOTIFICATION_SERVICE", "Watch Service Started");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //parse intent ang create connection
        String watch = intent.getStringExtra("watch");

        final String[] watchSplitted = watch.split("\\|");
        final String lastLs;
        final Connection connection = new Connection(
                watchSplitted[Connection.HOST],
                watchSplitted[Connection.USER],
                watchSplitted[Connection.PASSWORD],
                watchSplitted[Connection.PORT]
        );

        if (watchSplitted[watchSplitted.length - 1] == watchSplitted[PATH_POSITION]) {
            lastLs = "";
        } else {
            lastLs = watchSplitted[watchSplitted.length - 1];
        }


        final CommandExecuter commandExecuter = new CommandExecuter();
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Result result = new Result();
                boolean isChangedFlag = false;
                while (!isChangedFlag) {
                    commandExecuter.executeCommand(connection, "ls " + watchSplitted[PATH_POSITION], result, getApplicationContext());
                    while (result.isWaiting()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                    }
                    if (!result.getOutput().equals(NO_CONNECTION_STR)) {
                        if (!result.getOutput().replace("\n", "").equals(lastLs.replace("\n", ""))) {
                            notificationManager.notify(1, notifyUser("Something changed!", "Something changed in " + watchSplitted[PATH_POSITION]));
                            System.out.println("Output: " + result.getOutput());
                            System.out.println("Watch: " + watchSplitted[watchSplitted.length - 1]);
                            System.out.println("Something changed in " + watchSplitted[PATH_POSITION]);
                            isChangedFlag = true;
                        } else {
                            try {
                                Thread.sleep(15000);
                            } catch (InterruptedException e) {
                            }
                            continue;
                        }
                    }
                }
            }
        }).start();
        return START_NOT_STICKY;
    }

    private Notification notifyUser(String title, String text) {
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.drawable.sshannon_icon)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();

        return notification;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
