package com.example.sshannon.utils;

public abstract class Constants {
    //SHARED PREFERENCES
    public static final String CONNECTION_LIST = "connectionList";
    public static final String CONNECTIONS = "connections";
    public static final String WATCHES = "watches";
    public static final String CURRENT_WATCH = "currentWatch";

    //SERVER ICON POSITION
    public static final int ARCH = 0;
    public static final int CENTOS = 1;
    public static final int DEBIAN = 2;
    public static final int FEDORA = 3;
    public static final int FREEBSD = 4;
    public static final int MAC = 5;
    public static final int MAGEIA = 6;
    public static final int MINTOS = 7;
    public static final int OPENSUSE = 8;
    public static final int REDHAT = 9;
    public static final int SLACKWARE = 10;
    public static final int UBUNTU = 11;
    public static final int LINUX = 12;
    public static final int DEFAULT = 13;

    //SPLIT CONSTANTS
    public static final int PATH_POSITION = 5;

    public static final String NO_CONNECTION_STR = "No internet connection available!";
}
