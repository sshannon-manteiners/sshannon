package com.example.sshannon.utils;

import com.jcraft.jsch.SftpProgressMonitor;

public class ProgressMonitor implements SftpProgressMonitor {
    private long max = 0;
    private long count = 0;
    private long percent = 0;

    public ProgressMonitor() {
    }

    public void init(int op, java.lang.String src, java.lang.String dest, long max) {
        this.max = max;
    }

    public boolean count(long bytes) {
        this.count += bytes;
        long percentNow = this.count * 100 / max;
        if (percentNow > this.percent) {
            this.percent = percentNow;
        }
        return (true);
    }

    public void end() {
    }
}