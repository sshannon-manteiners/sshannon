package com.example.sshannon.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.sshannon.server.Connection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public abstract class SharedPreferencesHandler {
    public static void deleteConnection(Connection connection, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.CONNECTION_LIST, Context.MODE_PRIVATE);

        Set<String> allConnections = new HashSet<>();
        if (sharedPreferences != null)
            allConnections = new HashSet<>(sharedPreferences.getStringSet(Constants.CONNECTIONS, new HashSet<String>()));

        Set<String> currentConnections = new HashSet<>();
        for (String conn : allConnections) {
            if (conn.equals(connection.createConnectionIdentifier()))
                continue;
            currentConnections.add(conn);
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(Constants.CONNECTIONS, currentConnections);
        editor.apply();
    }

    public static void addConnection(Connection connection, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.CONNECTION_LIST, Context.MODE_PRIVATE);

        String connectionIdentifier = connection.createConnectionIdentifier();

        Set<String> connectionsIds = new HashSet<>();
        if (sharedPreferences != null) {
            connectionsIds = new HashSet<>(sharedPreferences.getStringSet(Constants.CONNECTIONS, new HashSet<String>()));
        }
        connectionsIds.add(connectionIdentifier);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(Constants.CONNECTIONS, new HashSet<>(connectionsIds));
        editor.apply();
    }

    private static Set<String> getConnectionIdentifiers(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.CONNECTION_LIST, Context.MODE_PRIVATE);

        Set<String> connectionsIds = new HashSet<>();
        if (sharedPreferences != null) {
            connectionsIds = new HashSet<>(sharedPreferences.getStringSet(Constants.CONNECTIONS, new HashSet<String>()));
        }

        return connectionsIds;
    }

    public static ArrayList<Connection> getConnections(Context context) {
        ArrayList<Connection> retArray = new ArrayList<>();

        Set<String> connectionsIds = getConnectionIdentifiers(context);

        for (String connString : connectionsIds) {
            Connection connection = fromStringToConnection(connString);
            retArray.add(connection);
        }

        return retArray;
    }

    public static Connection fromStringToConnection(String connectionIdentifier) {
        String host;
        String user;
        String password;
        String port;
        int icon;

        host = connectionIdentifier.split("\\|")[0];
        user = connectionIdentifier.split("\\|")[1];
        password = connectionIdentifier.split("\\|")[2];
        port = connectionIdentifier.split("\\|")[3];
        icon = Integer.parseInt(connectionIdentifier.split("\\|")[4]);

        Connection newConnection = new Connection(host, user, password, port, icon);
        return newConnection;
    }

    public static void editConnection(Connection oldConnection, Connection editedConnection, Context context) {
        Set<String> allConnections = getConnectionIdentifiers(context);

        for (String conn : allConnections) {
            if (conn.equals(oldConnection.createConnectionIdentifier())){
                allConnections.remove(conn);
                allConnections.add(editedConnection.createConnectionIdentifier());
                break;
            }
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.CONNECTION_LIST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(Constants.CONNECTIONS, new HashSet<>(allConnections));
        editor.apply();
    }

    public static void saveWatch(Connection connection, String path, String currentStructure, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.WATCHES, Context.MODE_PRIVATE);

        String watchString = WatchHandler.createWatchId(connection, path, currentStructure);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(Constants.CURRENT_WATCH, watchString);
        editor.apply();
    }

    public static String getCurrentWatch(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.WATCHES, Context.MODE_PRIVATE);

        String currentWatch = null;
        if (sharedPreferences != null) {
            currentWatch = sharedPreferences.getString(Constants.CURRENT_WATCH, null);
        }

        return currentWatch;
    }
}
