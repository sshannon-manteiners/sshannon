package com.example.sshannon.utils;

import com.example.sshannon.server.Connection;

public abstract class WatchHandler {
    public static String createWatchId(Connection connection, String path, String currentStructure) {
        String watchString = connection.createConnectionIdentifier() + "|" + path + "|" + currentStructure;
        return watchString;
    }
}
